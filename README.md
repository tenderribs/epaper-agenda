# Epaper Agenda

![Finished Epaper](./IMG_20211115_202836.jpg)

Display the daily schedule from a Caldav Server on an Epaper display. Create events in Google Calendar or whatever and show them on an Epaper Display. This specific setup is connected to my Nextcloud calendar and displays the classes I am taking at ETH.

### Config for your setup
Some fields in main.cpp need to be changed. These include the URL to your calendar server, the ID of the calendar and the Wifi password. Also download the root CA certificate of the webserver and replace the one in `ca_cert.h`.

## Hardware
Buy a Lilygo EPD47 from Aliexpress. For our intents and purposes this is an all-in-one Epaper display with built in Wifi.
[https://github.com/Xinyuan-LilyGO/LilyGo-EPD47](https://github.com/Xinyuan-LilyGO/LilyGo-EPD47)

## Software
### Installation
I am using nvim to edit the text files. Formattting options are specified in the `.clang-format` file. Generate a `compile_commands.json` compilation database for ccls using

```sh
intercept-build make all
```

### Build
A full list of options is obviously in the `Makefile`. Here are some common ones:

```sh
intercept-build make upload
```
