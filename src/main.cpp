#include <Arduino.h>
#include <ArduinoJson.h>
#include <HTTPClient.h>
#include <WiFiClientSecure.h>
#include <time.h>

#include <algorithm>
#include <vector>

#include "WiFi.h"
#include "ca_cert.h"
#include "epd_driver.h"
#include "firasans.h"

struct Event {
    String title;
    String start;
    String end;
};

struct Calendar {
    const char *id;
    String title;
    String payload;
    std::vector<Event> events;
};

const char *ssid = "it burns when IP";
const char *password = "6503269462";

const char *ntpServer = "pool.ntp.org";
const long gmtOffset_sec = 3600;
const int daylightOffset_sec = 3600;

RTC_DATA_ATTR struct tm now;  // keep value during deep sleep cycle

Calendar calendar = {
    .id = "X5SaesnBDJpekwPM", .title = "", .payload = "", .events = {}};

uint8_t *framebuffer;
int cursor_x = 20;
int cursor_y = 60;

constexpr uint8_t m_t_title = 50;  // title margin from left and top of screen
constexpr uint8_t t_height = 20;   // Title Height

constexpr uint8_t m_l_event = 40;
constexpr uint8_t e_height = 120;

/**
 * Initialize every single pixel in buffer as white
 *
 * @see https://en.wikipedia.org/wiki/Framebuffer
 */
void startEpd() {
    epd_init();

    framebuffer =
        (uint8_t *)ps_calloc(sizeof(uint8_t), EPD_WIDTH * EPD_HEIGHT / 2);
    memset(framebuffer, 0xFF, EPD_WIDTH * EPD_HEIGHT / 2);
}

void wifiConnect() {
    WiFi.begin(ssid, password);

    while (WiFi.status() != WL_CONNECTED) {
        delay(500);
    }
}

/**
 * @brief build http GET request for the calendar events of next three days
 *
 * This restriction avoids loading massive string with all events into memory
 */
const String query(const String &calendarID) {
    const time_t t_now = mktime(&now);  // [epoch time] = s
    const time_t today_start = t_now - (t_now % 86400);
    const time_t today_end = today_start + 86400;

    return "https://cloud.markmarolf.com/remote.php/dav/public-calendars/" +
           calendarID + "/?export&start=" + today_start + "&end=" + today_end +
           "&expand=1&accept=jcal&componentType=VEVENT";
}

void httpGetRequest() {
    HTTPClient http;
    http.begin(query(calendar.id).c_str(), root_ca);
    int response = http.GET();

    if (response > 0) {
        calendar.payload = http.getString();
    }

    http.end();
}

void write(const char *text) {
    write_string((GFXfont *)&FiraSans, text, &cursor_x, &cursor_y, framebuffer);
}

void drawTitle() {
    // horizontal line
    epd_draw_line(0, m_t_title + t_height, EPD_WIDTH, m_t_title + t_height, 0x0,
                  framebuffer);
    epd_draw_line(0, m_t_title + t_height + 1, EPD_WIDTH,
                  m_t_title + t_height + 1, 0x0, framebuffer);

    cursor_x = m_t_title;
    cursor_y = m_t_title;
    write("Mark's ETH Schedule");
}

const tm castToTime(const String &dateString) {
    tm t;
    sscanf(dateString.c_str(), "%d-%d-%dT%d:%dZ", &t.tm_year, &t.tm_mon,
           &t.tm_mday, &t.tm_hour, &t.tm_min);
    return t;
}

// The hours from the ecalendar are returned in GMT, which has to be adjusted
// for EU/Zurich
String incrementHour(const String &dateString) {
    tm t = castToTime(dateString);
    return String(dateString.substring(0, 11) + (t.tm_hour + 2) +
                  dateString.substring(13));
}

void deserializePayload() {
    DynamicJsonDocument doc(3 * 1024);  // 3kb
    deserializeJson(doc, calendar.payload);

    for (size_t i = 0; i < doc[2].size(); i++) {
        Event event = {};

        // first cast JSONDoc to const char to infer type
        const char *title = doc[2][i][1][2][3];
        const char *start = doc[2][i][1][1][3];
        const char *end = doc[2][i][1][3][3];

        // then unambiguous type can be cast to Arduino String
        event.title = String(title);
        event.start = incrementHour(String(start));
        event.end = incrementHour(String(end));

        calendar.events.push_back(event);
    }
}

bool compareStartTimes(const Event &e1, const Event &e2) {
    const tm a = castToTime(e1.start);
    const tm b = castToTime(e2.start);

    // multiply by 100 to move hours over by two decimals
    return (a.tm_hour * 100 + a.tm_min) < (b.tm_hour * 100 + b.tm_min);
}

void drawEvents() {
    deserializePayload();

    std::sort(calendar.events.begin(), calendar.events.end(),
              compareStartTimes);

    for (uint8_t e = 0; e < calendar.events.size(); e++) {
        cursor_x = m_l_event;
        cursor_y = (e + 1) * e_height;
        write(calendar.events[e].title.c_str());

        cursor_x = m_l_event;  // reset cursor to left part of screen
        String start = calendar.events[e].start.substring(11, 16);
        String end = calendar.events[e].end.substring(11, 16);
        write((start + " - " + end).c_str());
    }
}

void updateDisplay() {
    epd_poweron();
    epd_clear();

    drawTitle();
    drawEvents();

    // finally draw the framebuffer onto the screen
    epd_draw_grayscale_image(epd_full_screen(), framebuffer);
    epd_poweroff_all();
};

/**
 * @brief called on boot. No loop function because sleep after syncing and
 * writing to screen
 */
void setup() {
    Serial.begin(115200);
    startEpd();

    wifiConnect();
    configTime(gmtOffset_sec, daylightOffset_sec,
               ntpServer);  // sync with NTP
    getLocalTime(&now);

    if (WiFi.status() == WL_CONNECTED) {
        httpGetRequest();
    }

    updateDisplay();

    esp_sleep_enable_timer_wakeup(3 * 3600 * 1e6);  // Sync every x hours
    esp_deep_sleep_start();
}

void loop(){};  // Program never gets this far because it is in deep sleep
